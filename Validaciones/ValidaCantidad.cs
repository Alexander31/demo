﻿namespace Validaciones
{
    public class ValidaCantidad
    {
        public bool MayorACero(int cantidad)
        {
            bool respuesta = false;
            if (cantidad > 0)
            {
                respuesta = true;
            }
            return respuesta;
        }
    }
}