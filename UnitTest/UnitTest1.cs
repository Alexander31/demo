﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Validaciones;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        ValidaCantidad val = new ValidaCantidad();

        [TestMethod]
        public void MayorACero()
        {
            //Comentario
            int numeroEnviado = 1;
            bool ValorEsperado = true;
            bool ValorRecibido = val.MayorACero(numeroEnviado);
            Assert.AreEqual(ValorEsperado, ValorRecibido);
        }
    }
}
